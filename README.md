# Simple JSF

Simple examples using JSF

## Running

To run, build this project using Maven and deploy to Apache Tomcat 8.

NetBeans will do everything with "Clean/Build" and "Run".

If want to use the command line:

1. build the project with:
```
mvn clean install
```

2. copy the 'war' file to Apache Tomcat's deployment folder:
```
cp target/simple-jsf-<version>.war <tomcat-folder>/webapps
```

3. start the Tomcat Server:
```
cd <tomcat-folder>/bin
./startup.sh
```

4. open your browser in the following address:
```
http://localhost:8080/simplejsf/
```