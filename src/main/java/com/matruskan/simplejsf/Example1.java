package com.matruskan.simplejsf;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author matheus
 */
@Named
@RequestScoped
public class Example1 {
    private static final Logger LOGGER = LoggerFactory.getLogger(Example1.class);
    @Inject
    private Message message;
    private String name;
    private String lastName;
    
    @PostConstruct
    public void init() {
        name = "Peter";
    }

    public String getMessage() {
        return message.getMessage();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public void action() {
        LOGGER.info("Form submited! Name: {}, Last Name: {}", name, lastName);
        FacesContext.getCurrentInstance().addMessage("mainForm", new FacesMessage("Form submited!", "Name: \"" + name + "\" Last Name: \"" + lastName+ '\"'));
    }
}
