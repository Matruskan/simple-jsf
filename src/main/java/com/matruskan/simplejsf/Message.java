package com.matruskan.simplejsf;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author matheus
 */
@Named
@RequestScoped
public class Message {
    private String message = "Hello World";
    
    public String getMessage() {
        return message;
    }
}
